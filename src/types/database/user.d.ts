import { DBUserBase } from '../../lib/types/database/user';

export interface RemoteToken {
	token: string;
	username: string;
}
